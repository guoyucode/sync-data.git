use std::{sync::Mutex, time::Duration};

use fast_able::{stock_pool::StockPool, unsafe_cell_type::U};

/*
fast-able = "=1.13.22"

new_empty: 74.560897ms
insert: 164ns
get: 122ns

new_empty: 74.77127ms
insert: 171ns
get: 150ns

new_empty: 76.095701ms
insert: 154ns
get: 120ns

----------------------------------------
use Box type in stock pool:
fast-able = "=1.13.25"

new_empty: 28.281432ms
insert: 356ns
get: 119ns

new_empty: 30.021348ms
insert: 344ns
get: 139ns

new_empty: 28.339468ms
insert: 336ns
get: 200ns

new_empty: 28.096745ms
insert: 349ns
get: 137ns

new_empty: 29.169263ms
insert: 368ns
get: 130ns
*/
fn main() {
    println!("tst start...");
    std::env::set_var("RUST_LOG", "debug");

    // 预热
    for i in 0..99999999_i64 {
        let _s = (i + 100) * 100 % 100 + 10000;
    }
    for i in 0..99999999_i64 {
        let _s = (i + 101) * 100 % 100 + 10000;
    }
    for i in 0..99999999_i64 {
        let _s = (i + 102) * 100 % 100 + 10000;
    }

    // 分配内存 测试
    let count = 100_i32;
    let mut es = Duration::from_nanos(0);
    for _ in 0..count {
        let start = std::time::Instant::now();
        let _d = StockPool::<Data>::new_empty();
        es += start.elapsed();
    }
    println!("new_empty: {:?}", es / count as u32);

    // 插入数据 测试
    let count = 1000000_i32;
    let pool = StockPool::<Data>::new_empty();
    let mut es = Duration::from_nanos(0);
    for i in 0..count {
        let d = Data::default();
        let start = std::time::Instant::now();
        pool.insert(i, d);
        es += start.elapsed();
    }
    println!("insert: {:?}", es / count as u32);

    // 获取数据 测试
    let mut count = 0;
    let mut es = Duration::from_nanos(0);
    for i in 100_0001..109_0000_i32 {
        count += 1;
        let start = std::time::Instant::now();
        let _d = pool.get(i).unwrap();
        es += start.elapsed();
    }
    println!("get: {:?}", es / count as u32);
}

#[derive(Debug, Default)]
struct Data {
    a: i32,
    aa: U<i32>,
    b: i32,
    bb: U<i32>,
    c: i32,
    cc: U<i32>,
    d: i32,
    dd: U<i32>,
    e: i32,
    ee: U<i32>,
    f: i32,
    ff: U<i32>,
    g: i32,
    g_lock: Mutex<()>,
    h: i32,
    h_lock: Mutex<()>,
    i: i32,
    i_lock: Mutex<()>,
    j: i32,
    j_lock: Mutex<()>,
    k: i32,
    k_lock: Mutex<()>,
    l: i32,
    l_lock: Mutex<()>,
    m: i32,
    m_lock: Mutex<()>,
    n: i32,
    n_lock: Mutex<()>,
    o: i32,
    o_lock: Mutex<()>,
    p: i32,
    p_lock: Mutex<()>,
    q: i32,
    q_lock: Mutex<()>,
    r: i32,
    r_lock: Mutex<()>,
    s: i32,
    s_lock: Mutex<()>,
    t: i32,
    t_lock: Mutex<()>,
    u: i32,
    u_lock: Mutex<()>,
    v: i32,
    v_lock: Mutex<()>,
    w: i32,
    w_lock: Mutex<()>,
    x: i32,
    x_lock: Mutex<()>,
    y: i32,
    y_lock: Mutex<()>,
    z: i32,
    z_lock: Mutex<()>,
}
