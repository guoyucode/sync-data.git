#![allow(non_snake_case)]

#[macro_use]
extern crate log;

pub mod defer;
pub mod error;

pub mod map_btree;
pub mod map_hash;

#[path = "vec2.rs"]
pub mod vec;

pub mod wg;

pub mod statis;

pub mod unsafe_cell_type;

pub mod stock_pool;
pub mod fast_thread_pool;

pub mod elapsed_time;

pub mod static_type;

pub mod cache;

pub mod num_log;

/// 缓存队列
pub mod cache_queue;