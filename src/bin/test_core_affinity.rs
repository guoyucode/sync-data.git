use fast_able::fast_thread_pool::utils::{use_last_core, use_last_core2};
use log::{error, info};
use std::{fs, fs::OpenOptions};

fn main() {
    // 简单打印日志，不使用env_logger初始化
    println!("开始测试核心分配逻辑");
    
    // 删除旧的核心亲和性文件以确保干净的测试环境
    println!("尝试删除旧的核心亲和性文件");
    fast_able::fast_thread_pool::init(true);
    
    // 测试多次调用核心分配函数，查看分配的顺序和重复情况
    println!("\n第一轮分配测试");
    
    // 测试分配多个核心
    let core1 = use_last_core2("测试1多核心", 5);
    println!("测试1分配的核心: {:?}", core1);
    
    // 测试单核心分配
    let core2 = use_last_core("测试2单核心");
    println!("测试2分配的核心: {:?}", core2);
    
    let core3 = use_last_core("测试3单核心");
    println!("测试3分配的核心: {:?}", core3);
    
    let core4 = use_last_core("测试4单核心");
    println!("测试4分配的核心: {:?}", core4);
    
    let core5 = use_last_core("测试5单核心");
    println!("测试5分配的核心: {:?}", core5);
    
    // 读取并显示.core_affinity文件内容
    println!("\n第一轮分配后的核心亲和性文件内容：");
    match fs::read_to_string(".core_affinity") {
        Ok(content) => {
            for line in content.lines() {
                println!("{}", line);
            }
        }
        Err(e) => println!("无法读取.core_affinity文件: {}", e),
    }
    
    // 测试第二轮分配，此时应该会循环使用核心
    println!("\n第二轮分配测试");
    
    let core6 = use_last_core2("测试6多核心", 3);
    println!("测试6分配的核心: {:?}", core6);
    
    let core7 = use_last_core("测试7单核心");
    println!("测试7分配的核心: {:?}", core7);
    
    let core8 = use_last_core("测试8单核心");
    println!("测试8分配的核心: {:?}", core8);

    let core9 = use_last_core2("测试9多核心", 20);
    println!("测试9分配的核心: {:?}", core9);

    let core10 = use_last_core2("测试10多核心", 5);
    println!("测试10分配的核心: {:?}", core10);

    let core11 = use_last_core2("测试11多核心", 5);
    println!("测试11分配的核心: {:?}", core11);

    let core12 = use_last_core("测试12单核心");
    println!("测试12分配的核心: {:?}", core12);
    
    // 读取并显示更新后的.core_affinity文件内容
    println!("\n第二轮分配后的核心亲和性文件内容：");
    match fs::read_to_string(".core_affinity") {
        Ok(content) => {
            for line in content.lines() {
                println!("{}", line);
            }
        }
        Err(e) => println!("无法读取.core_affinity文件: {}", e),
    }
    
    println!("\n核心分配逻辑测试完成");
}
