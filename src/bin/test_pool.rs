use std::sync::Once;

static INIT: Once = Once::new();

fn init() {
    INIT.call_once(|| {
        // 设置日志级别环境变量
        unsafe { std::env::set_var("RUST_LOG", "info") };
        // 不使用env_logger初始化，仅依赖库本身的日志实现
    });
}

fn main() {
    init();
}

// 简单测试程序，用于测试核心分配策略
/*
fn main() {
    init();
    
    println!("开始测试 use_last_core2 函数...");
    
    // 直接手动测试核心分配
    println!("手动测试核心分配：");
    
    // 获取3个核心
    let cores1 = use_last_core2("manual_test_1", 3);
    println!("第一次请求3个核心: {:?}", cores1);
    
    // 再次请求3个核心，应该从上次最后一个核心开始循环
    let cores2 = use_last_core2("manual_test_2", 3);
    println!("第二次请求3个核心: {:?}", cores2);
    
    // 请求更多核心
    let cores3 = use_last_core2("manual_test_3", 5);
    println!("第三次请求5个核心: {:?}", cores3);
    
    // 运行完整测试函数
    println!("\n运行完整测试函数:");
    test_use_last_core2_impl();
    
    println!("测试完成！");
}
*/
