use std::fmt::Debug;

/*
缓存队列, 可一边缓存数据, 一边使用数据, 缓存满时, 自动覆盖最旧的数据
*/
pub struct CacheQueue<T, const L: usize> {
    data: [T; L],
    add: usize,
    useed: usize,
}

impl<T: Default, const L: usize> CacheQueue<T, L> {
    pub fn new() -> Self {
        let data = std::array::from_fn(|_| T::default());
        CacheQueue {
            data: data,
            add: 0,
            useed: 0,
        }
    }
}

impl<T, const L: usize> CacheQueue<T, L> {
    pub fn push(&mut self, t: T) {
        self.data[self.add] = t;
        self.add += 1;
        if self.add >= L {
            self.add = 0;
        }
    }
}
